# -*- coding: utf-8 -*-


import random
import re
import pymorphy2
import os

import pickle


def normal(word, dictionary):    # ищет норм. форму слова в словаре, иначе применяет pymorphy2
    if word not in dictionary:
        dictionary[word] = morph.parse(word)[0].normal_form
    return dictionary[word]


def top_ngramms(frequency, length):   # отбор при помощи сортировки по популярности (первые 2500)
    pairs = [(frequency[key], key) for key in frequency]
    pairs.sort(reverse=True)
    pairs = pairs[:2500]
    frequency.clear()
    for (el1, el2) in pairs:
        frequency[el2] = el1 / length
    return frequency


def lemmatization(text, stop_words, dictionary):
    tokens = re.findall("[а-яА-ЯёЁ]+(?:[-][а-яА-ЯёЁ]+)*", text)   # деление текста на слова
    length = 0
    frequency = {}   # словарь для вероятностей юниграмм
    freq_bigramm = {}   # словарь для вероятнсотей биграмм
    word2 = ''

    for word in tokens:
        word.lower()
        word = normal(word, dictionary)   # приведение слова к нормальной форме
        #word = dictionary[word]
        if word not in stop_words:  # выбрасываем стоп-слова
            length += 1
            frequency[word] = frequency.get(word, 0) + 1    # меняем вероятность юниграммы word
            if word2 != '':
                bigramm = word2 + ' ' + word
                freq_bigramm[bigramm] = freq_bigramm.get(bigramm, 0) + 1    # меняем вероятность биграммы
            word2 = word

    frequency = top_ngramms(frequency, length)   # отбираем самые популярные юниграммы
    freq_bigramm = top_ngramms(freq_bigramm, length)   # отбираем самые популярные биграммы
    return [frequency, freq_bigramm]


def cosine(freq1, freq2):    # вычисляет косинус угла между двумя векторами, как скалярное произведение
    sc = 0                   # делить на произведение модулей
    len_freq1 = 0
    len_freq2 = 0
    for word in freq2:
        sc += freq1.get(word, 0) * freq2[word]   # скалярное произведение
        len_freq2 += (freq2[word] * freq2[word])   # длина одного вектора
    for word in freq1:
        len_freq1 += (freq1[word] * freq1[word])   # длина другого вектора
    cos = sc / ((len_freq1 ** (1/2)) * (len_freq2 ** (1/2)))
    return cos


def find_author(frequency, freq, a, b):
    ideal = -10
    for name in frequency:
        for text in frequency[name]:   # этими циклами перебираются все тексты обучающей выборки
            cos1 = cosine(text[0], freq[0])   # косинус между векторами юниграмм для данных текстов
            cos2 = cosine(text[1], freq[1])   # косинус между векторами биграмм тех же текстов
            if a * cos1 + b * cos2 >= ideal:  # поиск наибольшей линейной комбинации для определения автора
                ideal = a * cos1 + b * cos2
                res = name
    return res


def remember(dictionary):    # запомнить словарь нормальных форм некоторых слов
    with open('data.txt', 'wb') as f:
        pickle.dump(dictionary, f)

morph = pymorphy2.MorphAnalyzer()

stop_words = ['без', 'более', 'бы', 'быть', 'ведь', 'весь', 'вдоль', 'вместо', 'вне', 'вниз', 'внизу', 'внутри', 'во',
              'вокруг', 'вот', 'все', 'всегда', 'всего', 'вы', 'где', 'да', 'давай', 'давать', 'даже', 'для', 'до',
              'достаточно', 'его', 'ее', 'её', 'если', 'есть', 'ещё', 'же', 'за', 'за исключением', 'здесь', 'из',
              'из-за', 'или', 'им', 'иметь', 'их', 'как', 'как-то', 'кто', 'когда', 'кроме', 'кто', 'ли', 'либо',
              'мне', 'может', 'мои', 'мой', 'мы', 'на', 'навсегда', 'над', 'надо', 'наш', 'не', 'него', 'неё', 'нет',
              'ни', 'но', 'ну', 'об', 'однако', 'он', 'она', 'они', 'оно', 'от', 'отчего', 'очень', 'по', 'под',
              'после', 'потому', 'потому что', 'почти', 'при', 'про', 'снова', 'со', 'так', 'также', 'такие', 'такой',
              'там', 'те', 'тем', 'то', 'того', 'тоже', 'той', 'только', 'том', 'тут', 'ты', 'уже', 'хотя', 'чего',
              'чего-то', 'чей', 'чем', 'что', 'чтобы', 'чьё', 'чья', 'эта', 'эти', 'это','а', 'б', 'в', 'г', 'д', 'е',
              'ё', 'ж', 'з', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'э',
              'ь', 'ы', 'ю', 'я']   # список слов, которые не будут учтены при формировании словарей вероятностей

with open('data.txt', 'rb') as f:    # считать словарь нормальных форм некоторых слов
    dictionary = pickle.load(f)

percent = int(input("Введите процентное деление выборки: "))
a = int(input("Введите коэффициент для юниграмм: "))
b = int(input("Введите коэффициент для биграмм: "))

frequency_known = {}   # словарь вероятностей для текстов с известным автором
frequency_test = {}    # словарь вероятностей для текстов с неизвестным автором
names = os.listdir("Books")
#dictionary = {}
for name in names:
    books = os.listdir('Books/' + name)    # список книг автора
    teaching = random.sample(books, int(percent * len(books) / 100 + 0.5))   # тестирующая выборка текстов
    #books = filter(lambda x: x.endswith('.txt'), books)
    for book in books:
        f = open('Books/' + name + '/' + book)
        text = f.read()
        freq = lemmatization(text, stop_words, dictionary)   # получение векторов вероятностей для текста
        if book in teaching:
            frequency_known[name] = frequency_known.get(name, []) + [freq]
        else:
            frequency_test[name] = frequency_test.get(name, []) + [freq]

#remember(dictionary)
print("Определяются авторы...")
k = 0    # общее кол-во совпадений
t = 0    # общее количество текстов в обучающей выборке
for author in frequency_test:
    k1 = 0    # количество совпадений для данного автора
    t1 = 0    # количество текстов данного автора
    for text in frequency_test[author]:   # перебираем все тексты обучающей выборки для данного автора
        res = find_author(frequency_known, text, a, b)    # определяется автор
        k += 1
        k1 += 1
        if res == author:
            t += 1
            t1 += 1
    print(author, int(t1 / k1 * 100), "% из", k1)
print("Угадано:", t / k * 100, "% авторов")


'''
state = "2"            # закоментированный код предоставляет пользователю возможность определять автора текста,
while state == "2":    # у которого известно только название
    f = "1"
    while (f == "1"):
        file = input("Введите название текста: ")
        try:
            f = open('Tests/' + file + ".txt")
        except Exception:
            f = "1"
            print("Текст с таким названием не найден.")
    text = f.read()
    print("Определяется автор...")
    freq = lemmatization(text, stop_words, dictionary)

    res = find_author(frequency, freq)
    print("Предполагаемый автор:", res)
    print("\n")

    state = input("Если вы хотите завершить работу, введите 1. Для продолжения введите 2. ")
    while state != '1' and state != '2':
        state = input("Ваш ввод некорректен, введите еще раз: ")
'''
