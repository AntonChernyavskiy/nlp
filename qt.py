# -*- coding: utf-8 -*-
import sys
from PyQt4 import QtGui
from PyQt4 import QtCore
import urllib.request
import random
import re
import pymorphy2
import os
import pickle
from zipfile import *

class SecondWindow1(QtGui.QWidget):
    def __init__(self, parent=None,):
        super().__init__(parent, QtCore.Qt.Window)
        self.setWindowTitle(self.trUtf8(u'Ввод названия'))
        self.resize(300, 100)
        pal = self.palette()
        pal.setColor(QtGui.QPalette.Normal, QtGui.QPalette.Background,QtGui.QColor("#FFFACD"))
        pal.setColor(QtGui.QPalette.Inactive, QtGui.QPalette.Background, QtGui.QColor("#FFFACD"))
        self.setPalette(pal)
        vbox = QtGui.QVBoxLayout()
        hbox = QtGui.QHBoxLayout()
        self.frame = QtGui.QFrame(self)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.enter = QtGui.QLabel(u"Введите название текста:", self.frame)
        self.enter.setFont(QtGui.QFont("Arial", 10))
        vbox.addWidget(self.enter)
        self.Yield = QtGui.QLineEdit(u"", self.frame)
        hbox.addWidget(self.Yield)
        self.Yield.setFont(QtGui.QFont("Arial", 10))
        self.button = QtGui.QPushButton(u'OK', self)
        hbox.addWidget(self.button)
        vbox.addLayout(hbox)
        hbox = QtGui.QHBoxLayout()
        self.correct = QtGui.QLabel(u"", self.frame)
        vbox.addWidget(self.correct)
        QtCore.QObject.connect(self.button, QtCore.SIGNAL('clicked()'), self.changeText)
        self.string = QtGui.QLabel(u"Предполагаемый автор:", self.frame)
        self.string.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))
        pal = self.string.palette()
        hbox.addWidget(self.string)
        self.myText = QtGui.QLabel(u"", self.frame)
        self.myText.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))
        hbox.addWidget(self.myText)
        vbox.addLayout(hbox)
        self.button1 = QtGui.QPushButton(u'Считать еще один текст', self)
        vbox.addWidget(self.button1)
        QtCore.QObject.connect(self.button1, QtCore.SIGNAL('clicked()'), self.change)
        self.setLayout(vbox)

    def change(self):
        self.correct.setText(u"")
        self.myText.setText(u"")
        self.Yield.setText(u"")

    def changeText(self):
        self.entering = self.Yield.displayText()
        res = try_alone_text(self.entering)
        if res == 0:
            self.correct.setText(str(u"<font color=red>ВАШ ВВОД НЕКОРРЕКТЕН!!!</font>"))
        else:
            self.correct.setText(str(u"Определяется автор..."))
            result = open_alone_text(self.entering)
            self.correct.setText(str(""))
            self.myText.setText(str(result))


class SecondWindow2(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setGeometry(400, 300, 450, 200)
        self.setWindowTitle('Чтение из архива')
        self.textEdit = QtGui.QTextEdit()
        self.textEdit.setFont(QtGui.QFont("Arial", 10))
        self.setCentralWidget(self.textEdit)
        self.statusBar()
        self.setFocus()
        exit = QtGui.QAction(QtGui.QIcon('open.png'), 'Обзор', self)
        self.connect(exit, QtCore.SIGNAL('triggered()'), self.showDialog)
        clear = QtGui.QAction(QtGui.QIcon('open.png'), 'Очистить', self)
        self.connect(clear, QtCore.SIGNAL('triggered()'), self.clearing)
        menubar = self.menuBar()
        file = menubar.addMenu(u'&Выбрать архив')
        file.addAction(exit)
        file1 = menubar.addMenu(u'Очистить поле (для нового запуска)')
        file1.addAction(clear)

    def clearing(self):
        self.textEdit.setText(u"")

    def showDialog(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Обзор', '/home')
        if is_zipfile(filename):
            z = ZipFile(filename, 'r')
            z.testzip()
            self.textEdit.setText("Пожалуйста, ждите...")
            res = "Предполагаемые авторы: \n"
            for file in z.namelist():
                f = z.open(file)
                text = f.read().decode("cp1251")
                ans = alone_text(text)
                file = file.encode('cp437').decode('cp866')
                res += file + ":   " + ans + "\n"
            self.textEdit.setText(res)
            z.close()
        else:
            self.textEdit.setText(u"<font color=red>Выбранный файл НЕ ЯВЛЯЕТСЯ архивом!!!</font>")


class SecondWindow3(QtGui.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent, QtCore.Qt.Window)
        self.setWindowTitle(self.trUtf8(u'Ввод ссылки'))
        self.resize(300, 100)
        pal = self.palette()
        pal.setColor(QtGui.QPalette.Normal, QtGui.QPalette.Background,QtGui.QColor("#EEE8AA"))
        pal.setColor(QtGui.QPalette.Inactive, QtGui.QPalette.Background, QtGui.QColor("#EEE8AA"))
        self.setPalette(pal)
        vbox = QtGui.QVBoxLayout()
        hbox = QtGui.QHBoxLayout()
        self.frame = QtGui.QFrame(self)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.enter = QtGui.QLabel(u"Введите адрес текста в интернете:", self.frame)
        self.enter.setFont(QtGui.QFont("Arial", 10))
        vbox.addWidget(self.enter)
        self.Yield = QtGui.QLineEdit(u"", self.frame)
        self.Yield.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield)
        self.button = QtGui.QPushButton(u'OK', self)
        hbox.addWidget(self.button)
        vbox.addLayout(hbox)
        QtCore.QObject.connect(self.button, QtCore.SIGNAL('clicked()'), self.changeText)
        hbox = QtGui.QHBoxLayout()
        self.correct = QtGui.QLabel(u"", self.frame)
        vbox.addWidget(self.correct)
        self.string = QtGui.QLabel(u"Предполагаемый автор:", self.frame)
        self.string.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))
        hbox.addWidget(self.string)
        self.myText = QtGui.QLabel(u"", self.frame)
        self.myText.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))
        hbox.addWidget(self.myText)
        vbox.addLayout(hbox)
        self.button1 = QtGui.QPushButton(u'Считать еще один текст', self)
        vbox.addWidget(self.button1)
        QtCore.QObject.connect(self.button1, QtCore.SIGNAL('clicked()'), self.change)
        self.setLayout(vbox)

    def change(self):
        self.correct.setText(u"")
        self.myText.setText(u"")
        self.Yield.setText(u"")

    def changeText(self):
        self.entering = self.Yield.displayText()
        res = try_link(self.entering)
        if res == 0:
            self.correct.setText(str("<font color=red>ВАШ ВВОД НЕКОРРЕКТЕН!!!</font>"))
        else:
            res2 = try_decode(self.entering)
            if res2 == 0:
                self.correct.setText(str("<font color=red>НЕКОРРЕКТНАЯ КОДИРОВКА!!!</font>"))
            else:
                self.correct.setText(str("Определяется автор..."))
                result = text_from_internet(self.entering)
                self.correct.setText(str(""))
                self.myText.setText(str(result))


class Main_window(QtGui.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.secondWin1 = None
        self.secondWin2 = None
        self.secondWin3 = None
        QtGui.QWidget.__init__(self, parent)
        self.setWindowTitle(self.trUtf8(u'Определение автора текста'))
        self.resize(600, 150)
        pal = self.palette()
        pal.setColor(QtGui.QPalette.Normal, QtGui.QPalette.Background, QtGui.QColor("#FFDEAD"))
        pal.setColor(QtGui.QPalette.Inactive, QtGui.QPalette.Background, QtGui.QColor("#FFEFD5"))
        self.setPalette(pal)
        self.frame = QtGui.QFrame(self)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        vbox = QtGui.QVBoxLayout()
        self.var = QtGui.QLabel(u"<i>Выберете один из следующих вариантов считывания Вашего(их) текста(ов):<i>", self.frame)
        self.var.setFont( QtGui.QFont("Times new roman", 13))
        vbox.addWidget(self.var)
        self.button1 = QtGui.QPushButton(u'Считать отдельный текст (из папки Tests) по названию', self.frame)
        self.button1.setFont(QtGui.QFont("Arial", 10, QtGui.QFont.Bold))
        self.button2 = QtGui.QPushButton(u'Cчитать все тексты из архива', self.frame)
        self.button2.setFont(QtGui.QFont("Arial", 10, QtGui.QFont.Bold))
        self.button3 = QtGui.QPushButton(u'Cчитать текст из интернета', self.frame)
        self.button3.setFont(QtGui.QFont("Arial", 10, QtGui.QFont.Bold))
        vbox.addWidget(self.button1)
        vbox.addWidget(self.button2)
        vbox.addWidget(self.button3)
        QtCore.QObject.connect(self.button1, QtCore.SIGNAL('clicked()'), self.openWin1)
        QtCore.QObject.connect(self.button2, QtCore.SIGNAL('clicked()'), self.openWin2)
        QtCore.QObject.connect(self.button3, QtCore.SIGNAL('clicked()'), self.openWin3)
        self.setLayout(vbox)


    def openWin1(self):
        #if not self.secondWin1:
        self.secondWin1 = SecondWindow1(self)
        self.secondWin1.show()

    def openWin2(self):
        #if not self.secondWin2:
        self.secondWin2 = SecondWindow2(self)
        self.secondWin2.show()

    def openWin3(self):
        #if not self.secondWin3:
        self.secondWin3 = SecondWindow3(self)
        self.secondWin3.show()

    def closeEvent(self, event):
        reply = QtGui.QMessageBox.question(self, u'Выход',
        u"Вы уверены, что хотите выйти?", QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


morph = pymorphy2.MorphAnalyzer()


def normal(word, dictionary):    # ищет норм. форму слова в словаре, иначе применяет pymorphy2
    if word not in dictionary:
        dictionary[word] = morph.parse(word)[0].normal_form
    return dictionary[word]


def top_ngramms(frequency, length):   # отбор при помощи сортировки по популярности (первые 2500)
    pairs = [(frequency[key], key) for key in frequency]
    pairs.sort(reverse=True)
    pairs = pairs[:2500]
    frequency.clear()
    for (el1, el2) in pairs:
        frequency[el2] = el1 / length
    return frequency


def lemmatization(text):
    tokens = re.findall("[а-яА-ЯёЁ]+(?:[-][а-яА-ЯёЁ]+)*", text)   # деление текста на слова
    length = 0
    frequency = {}   # словарь для вероятностей юниграмм
    freq_bigramm = {}   # словарь для вероятнсотей биграмм
    word2 = ''

    for word in tokens:
        word.lower()
        word = normal(word, dictionary)   # приведение слова к нормальной форме
        #word = dictionary[word]
        if word not in stop_words:  # выбрасываем стоп-слова
            length += 1
            frequency[word] = frequency.get(word, 0) + 1    # меняем вероятность юниграммы word
            if word2 != '':
                bigramm = word2 + ' ' + word
                freq_bigramm[bigramm] = freq_bigramm.get(bigramm, 0) + 1    # меняем вероятность биграммы
            word2 = word

    frequency = top_ngramms(frequency, length)   # отбираем самые популярные юниграммы
    freq_bigramm = top_ngramms(freq_bigramm, length)   # отбираем самые популярные биграммы
    return [frequency, freq_bigramm]


def cosine(freq1, freq2):    # вычисляет косинус угла между двумя векторами, как скалярное произведение
    sc = 0                   # делить на произведение модулей
    len_freq1 = 0
    len_freq2 = 0
    for word in freq2:
        sc += freq1.get(word, 0) * freq2[word]   # скалярное произведение
        len_freq2 += (freq2[word] * freq2[word])   # длина одного вектора
    for word in freq1:
        len_freq1 += (freq1[word] * freq1[word])   # длина другого вектора
    cos = sc / ((len_freq1 ** (1/2)) * (len_freq2 ** (1/2)))
    return cos


def find_author(frequency, freq, a, b):
    ideal = -10
    for name in frequency:
        for text in frequency[name]:   # этими циклами перебираются все тексты обучающей выборки
            cos1 = cosine(text[0], freq[0])   # косинус между векторами юниграмм для данных текстов
            cos2 = cosine(text[1], freq[1])   # косинус между векторами биграмм тех же текстов
            if a * cos1 + b * cos2 >= ideal:  # поиск наибольшей линейной комбинации для определения автора
                ideal = a * cos1 + b * cos2
                res = name
    return res


def try_alone_text(file):
    try:
        f = open('Tests/' + file + ".txt")
    except Exception:
        return 0
    return 1


def open_alone_text(file):
    f = open('Tests/' + file + ".txt")
    text = f.read()
    res = alone_text(text)
    return res


def alone_text(text):
    if len(frequency_test) + len(frequency_known) == 0:
        prepare()
    freq = lemmatization(text)
    frequency = dict(list(frequency_test.items()) + list(frequency_known.items()))
    res = find_author(frequency, freq, a, b)
    return res


def try_link(link):
    try:
        f = urllib.request.urlopen(link);
    except Exception:
        return 0
    return 1


def try_decode(link):
    f = urllib.request.urlopen(link);
    try:
        text = f.read().decode(f.headers.get_content_charset())
    except Exception:
        return 0
    return 1


def text_from_internet(link):
    f = urllib.request.urlopen(link);
    text = f.read().decode(f.headers.get_content_charset())
    freq = lemmatization(text)
    if len(frequency_test) + len(frequency_known) == 0:
        prepare()
    frequency = dict(list(frequency_test.items()) + list(frequency_known.items()))
    res = find_author(frequency, freq, a, b)
    return res


def prepare():
    percent = 100
    names = os.listdir("Books")
    for name in names:
        books = os.listdir('Books/' + name)    # список книг автора
        teaching = random.sample(books, int(percent * len(books) / 100 + 0.5))   # тестирующая выборка текстов
        #books = filter(lambda x: x.endswith('.txt'), books)
        for book in books:
            f = open('Books/' + name + '/' + book)
            text = f.read()
            freq = lemmatization(text)   # получение векторов вероятностей для текста
            if book in teaching:
                frequency_known[name] = frequency_known.get(name, []) + [freq]
            else:
                frequency_test[name] = frequency_test.get(name, []) + [freq]


if __name__ == '__main__':
    global dictionary
    global frequency_known
    global frequency_test
    global stop_words
    global a, b
    a = 1
    b = 2
    stop_words = ['без', 'более', 'бы', 'быть', 'ведь', 'весь', 'вдоль', 'вместо', 'вне', 'вниз', 'внизу', 'внутри', 'во',
              'вокруг', 'вот', 'все', 'всегда', 'всего', 'вы', 'где', 'да', 'давай', 'давать', 'даже', 'для', 'до',
              'достаточно', 'его', 'ее', 'её', 'если', 'есть', 'ещё', 'же', 'за', 'за исключением', 'здесь', 'из',
              'из-за', 'или', 'им', 'иметь', 'их', 'как', 'как-то', 'кто', 'когда', 'кроме', 'кто', 'ли', 'либо',
              'мне', 'может', 'мои', 'мой', 'мы', 'на', 'навсегда', 'над', 'надо', 'наш', 'не', 'него', 'неё', 'нет',
              'ни', 'но', 'ну', 'об', 'однако', 'он', 'она', 'они', 'оно', 'от', 'отчего', 'очень', 'по', 'под',
              'после', 'потому', 'потому что', 'почти', 'при', 'про', 'снова', 'со', 'так', 'также', 'такие', 'такой',
              'там', 'те', 'тем', 'то', 'того', 'тоже', 'той', 'только', 'том', 'тут', 'ты', 'уже', 'хотя', 'чего',
              'чего-то', 'чей', 'чем', 'что', 'чтобы', 'чьё', 'чья', 'эта', 'эти', 'это','а', 'б', 'в', 'г', 'д', 'е',
              'ё', 'ж', 'з', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'э',
              'ь', 'ы', 'ю', 'я']   # список слов, которые не будут учтены при формировании словарей вероятностей
    frequency_known = {}
    frequency_test = {}
    with open('data.txt', 'rb') as f:    # считать словарь нормальных форм некоторых слов
        dictionary = pickle.load(f)

    app = QtGui.QApplication(sys.argv)
    window = Main_window()
    window.show()
    sys.exit(app.exec_())
